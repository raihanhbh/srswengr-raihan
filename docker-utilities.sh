#!/bin/bash

CLEANUP="cleanup"
KILL_ALL_IMAGES="kill"
RUN="run"

if [ "$#" -eq 0 ] || [ $1 = "-h" ] || [ $1 = "--help" ]; then
    echo "Usage: ./docker-utilities [OPTIONS] COMMAND [arg...]"
    echo "       ./docker-utilities [ -h | --help ]"
    echo ""
    echo "Options:"
    echo "  -h, --help    Prints usage."
    echo ""
    echo "Commands:"
    echo "  $CLEANUP            Removes stopped containers, followed by untagged images and then unused volumes."
    echo "  $KILL_ALL_IMAGES               Removes all images."
    echo "  $RUN                Build and run."
    exit
fi

cleanup() {
	echo "Cleaning up docker."
	remove_stopped_containers
	remove_untagged_images
	remove_unused_volumes
}

kill_all_images() {
	echo "Killing all images."
	remove_all_images
}

run() {
	echo "Building..."
	docker-compose build --force-rm
	echo "Running..."
	docker-compose up -d
}

remove_stopped_containers() {
	CONTAINERS="$(docker ps -a -f status=exited -q)"
	if [ ${#CONTAINERS} -gt 0 ]; then
		echo "Removing all stopped containers."
		docker rm $CONTAINERS
	else
		echo "All stopped containers has already been removed."
	fi
}

remove_all_images() {
	CONTAINERS="$(docker images -q)"
	if [ ${#CONTAINERS} -gt 0 ]; then
		echo "Removing all untagged images."
		docker rmi -f $CONTAINERS
	else
		echo "All untagged images has already been removed."
	fi
}

remove_untagged_images() {
	CONTAINERS="$(docker images -f "dangling=true" -q)"
	if [ ${#CONTAINERS} -gt 0 ]; then
		echo "Removing all untagged images."
		docker rmi $CONTAINERS
	else
		echo "All untagged images has already been removed."
	fi
}

remove_unused_volumes() {
	CONTAINERS="$(docker volume ls -qf dangling=true)"
	if [ ${#CONTAINERS} -gt 0 ]; then
		echo "Removing all unused volumes."
		docker volume rm $CONTAINERS
	else
		echo "All unused volumes has already been removed."
	fi
}

if [ $1 = $CLEANUP ]; then
	cleanup
	exit
fi

if [ $1 = $KILL_ALL_IMAGES ]; then
	kill_all_images
	exit
fi

if [ $1 = $RUN ]; then
	run
	exit
fi